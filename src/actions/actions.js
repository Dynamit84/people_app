import { startFromCapital } from "../utils/startFromCapital";

const CONNECTIONS = 'connections';
const CONNECTED = 'connected';
const BLACKLISTED = 'blacklisted';
const BLACKLIST = 'blacklisted';

export const fetchUsers = () => (dispatch) => {
	
	fetch('https://randomuser.me/api/?results=50&seed=db70b3ec56a640c1')
        .then(response => response.json())
		.then(data => data.results)
        .then(users => processRawUserData(users))
		.then(users => {
            dispatch(usersFetchSuccess(users));
            dispatch(usersAreLoading(false));
        })
		.catch(()=> dispatch(usersFetchHasErrored(true)));
};

const processRawUserData = (users) => {

    return users.map(user => {

        fillEmptyId(user);
        addFullNameProp(user);
        addProperty(user, CONNECTIONS, CONNECTED);
        addProperty(user, BLACKLIST, BLACKLISTED);

        return user;
    })
};

export const usersFetchHasErrored = (bool) => {
	
	return {
		type: 'FETCH_USERS_HAS_ERRORED',
		payload: bool
	};
};

export const usersAreLoading = (bool) => {
	
	return {
		type: 'USERS_ARE_LOADING',
		payload: bool
	};
};

export const usersFetchSuccess = (users) => {
	
	return {
		type: 'FETCH_USERS_SUCCESS',
		payload: users
	};
};

export const addConnection = (userData) => {

    return {
        type: 'ADD_CONNECTION',
        payload: userData
    };
};

export const removeConnection = (index) => {

    return {
        type: 'REMOVE_CONNECTION',
        payload: index
    }
};

export const removeBlacklisted = (index) => {

    return {
        type: 'REMOVE_BLACKLISTED',
        payload: index
    }
};

export const addToBlacklist = (userData) => {

    return {
        type: 'ADD_TO_BLACKLIST',
        payload: userData
    }
};

export const toggleDropdown = () => {
	
	return {
		type: 'SORT_TOGGLE'
	}
};

export const toggleProfile = () => {
	
	return {
		type: 'PROFILE_TOGGLE'
	}
};

export const toggleSearch = () => {

	return {
		type: 'SEARCH_TOGGLE'
	}
};

export const sortList = (sortBy) => {
	
	return {
		type: 'RE_SORT',
		payload: sortBy
	}
};

export const clearConnections = (users) => {
	const emptyConnections = removeConnections(users);

    return {
        type: 'CLEAR_CONNECTIONS',
		payload: emptyConnections
    }
};

export const loginSuccess = (profile) => {
	
	return {
		type: 'LOGIN_SUCCESS',
		payload: profile
	}
};

export const loginError = (err) => {
	return {
		type: 'LOGIN_ERROR',
        payload: err
	}
};

export const logoutSuccess = () => {
	return {
		type: 'LOGOUT_SUCCESS'
	}
};

export const changeSearchValue = (searchValue) => {
    return {
        type: 'SEARCH_VALUE',
        payload: searchValue
    }
};

export const setPager = (config) => {
    return {
        type: 'SET_PAGER',
        payload: config
    }
};

export const setPage = (pageNumber) => {
    return {
        type: 'SET_PAGE',
        payload: pageNumber
    }
};

const removeConnections = (users) => {
	return users.map(user => {
		if(user.connected) {
			user.connected = false;
			delete user.connectionDate;
		}
		
		return user;
	})
};

const fillEmptyId = (user) => {
    if(!user.id.value) {
        user.id.value = user.cell.replace(/[^0-9]/g, '');
    }

    return user;
};

const addProperty = (user, storageName, propertyName) => {
    const dataFromStorage = localStorage[storageName] ? JSON.parse(localStorage[storageName]) : [];
    const isStorageEmpty = dataFromStorage.length === 0;
    const findDataInStorage = !isStorageEmpty && dataFromStorage.find(data => user.id.value === data.id );
    const isUserInStorage = !!findDataInStorage;

    if(isStorageEmpty || !isUserInStorage) {
        user[propertyName] = false;

        return user;
    }

    switch (propertyName) {
        case 'connected':
            const { connectionDate } = findDataInStorage;
            user[propertyName] = true;
            user['connectionDate'] = connectionDate;
            break;
        case 'blacklisted':
            const { blacklistedDate } = findDataInStorage;
            user[propertyName] = true;
            user['blacklistedDate'] = blacklistedDate;
            break;
        default:
            return user;
    }

    return user;

};

const addFullNameProp = (user) => {
    const { name } = user;
    const capitalName = startFromCapital(name.first);
    const capitalSurname = startFromCapital(name.last);

    user.name.fullName = `${capitalName} ${capitalSurname}`;

    return user;
};



