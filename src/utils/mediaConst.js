const sizes = {
    desktop: 1024,
    tablet: 768,
    phoneLandScape: 480,
    phoneL: 375,
    phone: 320
};

export const device = {
    phone: `(min-width: ${sizes.phone}px)`,
    phoneL: `(min-width: ${sizes.phoneL}px)`,
    phoneLandScape: `(min-width: ${sizes.phoneLandScape}px)`,
    tablet: `(min-width: ${sizes.tablet}px)`,
    desktop: `(min-width: ${sizes.desktop}px)`,
};