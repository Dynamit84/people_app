export const populateRandomUsers = (users) => {
	
	
	if(users.length < 5) {
		
		return users;
	}
	
	let uniqueUsers = generateUnique(users.length);
	
	return uniqueUsers.map(userNum => users[userNum]);
};

let generateUnique = (max) => {
	
	let numArray =  [...Array(max-1).keys()];
	
	return shuffleArray(numArray).slice(0, 4);
};

let shuffleArray = (arr) => {
	let currentIndex = arr.length, randomIndex;
	
	while ( currentIndex !== 0 ) {
		
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex--;
		
		// And swap it with the current element.
		[arr[currentIndex], arr[randomIndex]] = [arr[randomIndex], arr[currentIndex]]
	}
	
	return arr;
};