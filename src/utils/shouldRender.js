export const USERS_REG_EXP = /^\/users\/\S+$/i;

export const shouldRender = regExp => {
    return !Boolean((window.location.pathname.match(regExp) || [false])[0]);
};