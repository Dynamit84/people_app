export const calculateConnectionDate = (date) => {
    const currentDate = Date.now();
    const oneDay = 1000*60*60*24;
    const diffMs = currentDate - date;
    const days = Math.round(diffMs/oneDay);
    const weeks = Math.round(days/7);
    const months = Math.round(weeks/4);
    const years = Math.round(months/12);

    switch (true) {
        case (days === 0):
            return 'today';
        case (days === 1):
            return '1 day ago';
        case (days > 1 && days < 7):
            return `${days} days ago`;
        case (weeks === 1):
            return '1 week ago';
        case (weeks > 1 && weeks < 4):
            return `${weeks} weeks ago`;
        case (months === 1):
            return '1 month ago';
        case (months > 1 && months < 12):
            return `${months} weeks ago`;
        case (years === 1):
            return '1 year ago';
        case (years > 1):
            return `${years} years ago`;
    }

};