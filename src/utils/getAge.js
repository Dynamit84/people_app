export const getAge = (dateOfBirth) => {
	const now = new Date();
	const date = new Date(dateOfBirth);
	
	return now.getFullYear() - date.getFullYear();
};