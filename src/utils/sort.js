export const sortNumbers = (array, prop) => {
	
	return array.sort((a, b) => {
		
		return b[prop] - a[prop];
	});
};

export const sortName = (array, prop) => {
	
	return array.sort((a, b) => {
		let string1 = a.name[prop],
			string2 = b.name[prop];
		
		if(string1 < string2) {
			
			return -1;
		}
		
		if(string1 > string2) {
			
			return 1;
		}
		
		return 0;
	});
};