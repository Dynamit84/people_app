import {keyframes} from 'styled-components';

export const leave = keyframes`
	from {
        opacity: 1;
        transform: scale(1);
    }
 
    to {
        opacity: 0;
        transform: scale(0);
    }
`;

export const appear = keyframes`
	0% {
    	transform: translateY(-1rem);
	}
	100% {
		opacity: 1;
    	transform: translateY(0);
	}
`;