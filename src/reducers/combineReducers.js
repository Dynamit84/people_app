import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { users } from "./users";
import { auth } from "./auth";
import { isLoading, isSortClosed, sortBy, isProfileClosed ,isSearchClosed, searchValue, usersToRender } from "./minorReducers";
import { pager } from "./pager";


export default combineReducers ({
    users,
	sortBy,
	isLoading,
	isSortClosed,
	isSearchClosed,
	auth,
	isProfileClosed,
    searchValue,
    usersToRender,
	pager,
	router: routerReducer
});