import auth0 from 'auth0-js';
import { AUTH_CONFIG } from "../utils/auth0-variables";

const checkTokenExpiry = () => {
	const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
	return new Date().getTime() < expiresAt;
};

const getProfile = () => {
	return JSON.parse(localStorage.getItem('profile')) || {};
};

const defaultAuthState = {
	isAuthenticated: checkTokenExpiry(),
	profile: getProfile(),
	error: '',
	auth0: new auth0.WebAuth({
		domain: AUTH_CONFIG.domain,
		clientID: AUTH_CONFIG.clientId,
		redirectUri: AUTH_CONFIG.callbackUrl,
		audience: `https://${AUTH_CONFIG.domain}/userinfo`,
		responseType: 'token id_token',
		scope: 'openid profile email'
	})
};

export const auth = (state = defaultAuthState, action) => {
	const { type, payload } = action;
	
	switch (type) {
		case 'LOGIN_SUCCESS':
			
			return {
				...state,
				isAuthenticated: true,
				profile: payload,
				error: ''
			};
		case 'LOGIN_ERROR':
			
			return {
				...state,
				isAuthenticated: false,
				profile: null,
				error: payload
			};
		case 'LOGOUT_SUCCESS':
			
			return {
				...state,
				isAuthenticated: false,
				profile: null
			};
		default:
			return state;
	}
};