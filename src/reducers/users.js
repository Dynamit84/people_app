export const users = (state = [], action) => {
	const { type, payload } = action;
	
	switch (type) {
		case 'FETCH_USERS_HAS_ERRORED':
			return state;
		case 'FETCH_USERS_SUCCESS':
			return [
				...state,
				...payload
			];
		case 'ADD_CONNECTION':
			return [
				...state.slice(0, payload.userIndex),
				{...state[payload.userIndex], connected: true, connectionDate: payload.connectionDate},
				...state.slice(payload.userIndex + 1)
			];
		case 'REMOVE_CONNECTION':
			const connection = state[payload];
			delete connection.connectionDate;
			return [
				...state.slice(0, payload),
				{...connection, connected: false},
				...state.slice(payload + 1)
			];
		case 'CLEAR_CONNECTIONS':
			return [...payload];
        case 'CLEAR_BLACKLIST':
            return [...payload];
		case 'ADD_TO_BLACKLIST':
			return [
				...state.slice(0, payload.userIndex),
				{...state[payload.userIndex], blacklisted: true, blacklistedDate: payload.blacklistedDate},
				...state.slice(payload.userIndex + 1)
			];
		case 'REMOVE_BLACKLISTED':
            const blacklisted = state[payload];
            delete blacklisted.blacklistedDate;
			return [
				...state.slice(0, payload),
				{...blacklisted, blacklisted: false},
				...state.slice(payload + 1)
			];
		default:
			return state;
	}
};