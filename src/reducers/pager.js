const defaultPager = {
    initialPage: 1,
    pageSize: 10,
    totalPages: 0,
    pages: [],
    startPage: 0,
    lastPage: 0,
    currentPage: 1
};

export const pager = (state = defaultPager, action) => {
    const { type, payload } = action;

    switch (type) {
        case 'CHANGE_PAGE_SIZE':
            return {
                ...state,
                pageSize: payload
            };

        case 'SET_PAGER':
            return {
                ...state,
                ...payload
            };

        case 'SET_PAGE':
            return {
                ...state,
                currentPage: payload
            };

        default:
            return state;
    }
};