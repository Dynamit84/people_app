export const isLoading = (state = true, action) => {
	const { type, payload } = action;
	
	if(type === 'USERS_ARE_LOADING') {
		
		return payload;
	}
	
	return state;
};

export const sortBy = (state = 'recently added', action) => {
	const { type, payload } = action;
	
	if(type === 'RE_SORT') {
		
		return payload;
	}
	
	return state;
};

export const isSortClosed = (state = true, action) => {
	const { type } = action;
	
	if(type === 'SORT_TOGGLE') {
		
		return !state;
	}
	
	return state;
};

export const isProfileClosed = (state = true, action) => {
	const { type } = action;
	
	if(type === 'PROFILE_TOGGLE') {
		
		return !state;
	}
	
	return state;
};

export const isSearchClosed = (state = true, action) => {
	const { type } = action;

	if(type === 'SEARCH_TOGGLE') {

		return !state;
	}

	return state;
};

export const searchValue = (state = '', action) => {
    const { type, payload } = action;

    if(type === 'SEARCH_VALUE') {

    	return payload;
	}

    return state;
};

export const usersToRender = (state = [], action) => {
    const { type, payload } = action;

    if(type === 'UPDATE_PAGE') {

        return [
            ...state,
            ...payload
        ];
    }

    return state;
};