import styled from 'styled-components';
import { device } from "../../utils/mediaConst";

export const Image = styled.img`
	width: 7.4rem;
    height: 7.4rem;
    box-sizing: border-box;
    background-clip: content-box;
    border: 0 solid transparent;
    border-radius: 49.9%;
    
    @media ${device.tablet} {
    	width: 10.4rem;
    	height: 10.4rem;
    }
`;