import { withRouter } from 'react-router-dom';
import React from 'react';
import styled from 'styled-components';
import { device } from "../../utils/mediaConst";

export const TransparentButton = styled.button`
	background-color: transparent;
    border: 0;
    cursor: pointer;
    outline: none;
    text-transform: capitalize;
`;

export const Button = styled(TransparentButton)`
    border-radius: .2rem;
    box-sizing: border-box;
    color: #0084bf;
    display: inline-block;
    font-family: inherit;
    height: 2.4rem;
    line-height: 2.4rem;
    overflow: hidden;
    position: relative;
    text-align: center;
    transition-property: background-color,box-shadow,color;
    transition-timing-function: cubic-bezier(0,0,.2,1);
    vertical-align: middle;
    z-index: 0;
    box-shadow: inset 0 0 0 .1rem #0084bf, inset 0 0 0 .2rem transparent, inset 0 0 0 .3rem transparent;
    font-size: 1.2rem;
    font-weight: 600;
    padding: 0 1.6rem;
    transition-duration: 167ms;
    text-decoration: none;
    width: 8rem;
    
    &:hover {
		background-color: rgba(0,115,177,.1);
		color: #0073b1;
		box-shadow: inset 0 0 0 .1rem #0084bf, inset 0 0 0 .2rem #0073b1, inset 0 0 0 .3rem transparent;
	}
	
	@media ${device.phoneL} {
		height: 2.8rem;
		font-size: 1.3rem;
		line-height: 2.8rem;
	}
`;

export const BiggerButton = styled(Button)`
    font-size: 1.3rem;
    font-weight: 400;
    letter-spacing: .03rem;
    
    @media ${device.tablet} {
    	margin-left: ${props => props.margin ? '1.5rem' : 0};
    	width: 17rem;
    	height: 3rem;
    	font-size: 1.5rem;
    }
`;

const ViewButton = ({ history, id, bigger }) => {
	
	if (bigger) {
		
		return <BiggerButton onClick={() => history.push(`users/${id}`)}>View</BiggerButton>;
	}
	
	return <Button onClick={() => history.push(`users/${id}`)}>View</Button>;
};

export const RemoveButton = ({onRemoveClick, margin, caption}) => {

	return <BiggerButton margin={margin} onClick={id => onRemoveClick(id)}>{caption}</BiggerButton>;
};

export default withRouter(ViewButton);