import React, {Component} from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import logo from '../../assets/img/logo.png';
import { Search } from "./Search";
import { Navigation } from "./Navigation";
import { ProfileIntro } from "./ProfileIntro";
import { connect } from 'react-redux';
import {toggleProfile, logoutSuccess, toggleSearch, changeSearchValue} from "../../actions/actions";
import Profile from "./Profile";
import { push } from 'react-router-redux';
import { WithMobileDetection } from "./WithMobileDetection";
import { MobileHeader } from "./MobileHeader/MobileHeader";
import { compose } from 'recompose';
import { CSSTransitionGroup } from 'react-transition-group';
import { shouldRender, USERS_REG_EXP } from "../../utils/shouldRender";
import { debounce } from "../../utils/debounce";

const Header = styled.header`
	height: 6rem;
	background: #555e67;
	display: flex;
	position: fixed;
	top: 0;
    z-index: 100;
    width: 100%;
    box-sizing: border-box;
    padding: 0 4rem;
	box-shadow: 0 0 1rem rgba(0,0,0,0.8);
	border-bottom: .4rem solid #1bbc9b;
`;

const LogoImage = styled.img`
	height: 3rem;
	margin-right: 1rem;
`;

export const Logo = styled.span`
	font-size: 2.7rem;
    color: #fff;
    font-weight: 600;
    text-transform: uppercase;
    display: inline-block;
    margin: 0 auto;
    line-height: 4.4rem;
`;

const StyledNavLink = styled(NavLink)`
	display: inline-flex;
	text-decoration: none;
	align-items: center;
	height: 100%;
	margin: 0 auto;
`;

export const FlexWrapper = styled.div`
	display: flex;
	align-items: center;
`;

const AlignedWrapper = FlexWrapper.extend`
	flex-grow: 2;
	align-items: center;
	position: relative;
`;

class AppHeader extends Component {
	
	openProfile() {
		this.props.toggleProfile();
	}

	handleMobileSearchClick() {
		const { toggleSearch, isSearchClosed, onSearchChange } = this.props;

		if (!isSearchClosed) {
            onSearchChange('');
		}
		toggleSearch();
	}

    onSearchChange(searchValue) {
        this.props.onSearchChange(searchValue);
    }
	
	logout() {
		const { push, logoutSuccess } = this.props;
		
		localStorage.removeItem('access_token');
		localStorage.removeItem('id_token');
		localStorage.removeItem('expires_at');
		localStorage.removeItem('profile');
		logoutSuccess();
		push('/login');
	}
	
	render() {
		const { profile, isProfileClosed, isAuthenticated, isMobile, isSearchClosed } = this.props;

		if(isMobile) {
			
			return (
				<CSSTransitionGroup
					transitionName="search"
					transitionEnter={true}
					transitionLeave={true}
					transitionEnterTimeout={400}
					transitionLeaveTimeout={400}>
					<MobileHeader isAuthenticated={isAuthenticated}
								  profile={profile}
								  handleSearchClick={this.handleMobileSearchClick.bind(this)}
								  isSearchClosed={isSearchClosed}
                                  onProfileClick={this.openProfile.bind(this)}
                                  isProfileClosed={isProfileClosed}
                                  onSearchChange={debounce(this.onSearchChange.bind(this), 800)}
                                  onLogOutClick={this.logout.bind(this)}/>
				</CSSTransitionGroup>
			)
		}
		
		return (
			<Header>
				<StyledNavLink to={'/home'}>
					<LogoImage src={logo} alt="InFriends"/>
					<Logo>infriends</Logo>
				</StyledNavLink>
				{
					isAuthenticated && (
						<AlignedWrapper>
							<Navigation/>
							{
                                shouldRender(USERS_REG_EXP) && <Search onSearchChange={this.onSearchChange.bind(this)}/>
							}
							<ProfileIntro onProfileClick={this.openProfile.bind(this)}
										  picture={profile.picture}
										  name={profile.givenName}/>
							{
								!isProfileClosed && <Profile onLogOutClick={this.logout.bind(this)} {...profile}/>
							}
						</AlignedWrapper>
					)
				}
			</Header>
		)
	}
}

function mapStateToProps(state) {
	
	return {
		profile: state.auth.profile,
        isProfileClosed: state.isProfileClosed,
        isSearchClosed: state.isSearchClosed,
		isAuthenticated: state.auth.isAuthenticated,
	}
}

function mapDispatchToProps(dispatch) {
	
	return {
		toggleProfile: (open) => dispatch(toggleProfile(open)),
		push: path => dispatch(push(path)),
		logoutSuccess: () => dispatch(logoutSuccess()),
		toggleSearch: () => dispatch(toggleSearch()),
        onSearchChange: (value) => dispatch(changeSearchValue(value))
	}
}

export default compose(connect(mapStateToProps, mapDispatchToProps), WithMobileDetection)(AppHeader);


