import React from 'react';
import styled from 'styled-components';
import {Image} from "../StyledComponents/Image";
import { TransparentButton } from "../StyledComponents/Buttons";
import {device} from "../../utils/mediaConst";

const SmallImage = styled(Image)`
	width: 3rem;
	height: 3rem;
	border: 1px solid #c7d1d8;
`;

const ProfileButton = styled(TransparentButton)`
	font-size: 1.3rem;
	color: #fff;
	height: 100%;
	min-width: 6rem;
	padding: 0;
	
	@media ${device.tablet} {
		min-width: 7rem;
	}
`;

const ButtonWrapper = styled.div`
	margin-left: 1rem;
	
	@media ${device.tablet} {
		margin: 0;
		right: -1.2rem;
		top: 2.8rem;
	}
`;

const ButtonContent = styled.div`
	position: relative;
	width: 100%;
    height: 100%;
    justify-content: space-between;
    display: flex;
	align-items: center;
	
	&:after {
		content: '';
		position: absolute;
		width: 0;
		height: 0;
		right: 1.5rem;
    	top: 2.2rem;
		border-left: .4rem solid transparent;
		border-right: .4rem solid transparent;
		border-top: .4rem solid #fff;
	}
`;

export const ProfileIntro = ({name, picture, onProfileClick}) => {
	return (
		<ButtonWrapper className={'ignore-react-onclickoutside'}>
			<ProfileButton onClick={() => onProfileClick()}>
				<ButtonContent>
                    <SmallImage src={picture}/>
                    {
                        name && <p>{name}</p>
                    }
				</ButtonContent>
			</ProfileButton>
		</ButtonWrapper>
	)
};