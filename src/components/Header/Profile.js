import React, { Component } from 'react';
import styled from 'styled-components';
import { TransparentButton } from "../StyledComponents/Buttons";
import onClickOutside from 'react-onclickoutside';
import { connect } from 'react-redux';
import { toggleProfile } from "../../actions/actions";
import { compose } from 'recompose';
import { device } from "../../utils/mediaConst";
import { NavLink } from 'react-router-dom';

const ProfileImage = styled.img`
	box-shadow: 0 .2rem .5rem rgba(0,0,0,.15);
    border: .2rem solid #fff;
    border-radius: 100%;
    display: inline;
    height: 6rem;
    width: 6rem;
    margin: 0;
    padding: 0;
`;

const ProfileContainer = styled.div`
	position: absolute;
	z-index: 110;
	box-shadow: 0 .4rem .8rem rgba(0,0,0,.15);
	background-color: #fff;
    background-clip: padding-box;
    right: 0;
    text-align: left;
    width: 100%;
    top: 4.4rem;
    
    @media ${device.tablet} {
        border-radius: .2rem;
        width: 28.8rem;
        top: 5.7rem;
    
        &:before, &:after {
            content: '';
            display: block;
            position: absolute;
            top: -2rem;
            right: 3rem;
            border: 10px solid transparent;
            border-bottom-color: rgba(0,0,0,.15);
        }
        
        &:after {
            top: -1.9rem;
            border-bottom-color: #fff;
        }
}
`;

const SignOutButton = styled(TransparentButton)`
	display: block;
	width: 100%;
	font-weight: 700;
	font-size: 1.5rem;
	text-align: left;
	padding: 0;
	
	&:hover {
		background-color: rgba(0,0,0,.1);
	}
`;

const InfoContainer = styled.div`
	padding: 1.5rem 2.4rem;
	display: flex;
    align-items: center;
`;

const Wrapper = styled.div`
    flex-grow: 2;
    margin-left: 1rem;
`;

const Title3 = styled.h3`
    color: rgba(0,0,0,.9);
    margin: 0;
    font-size: 1.8rem;
`;

const EmailText = styled.p`
    font-size: 1.3rem;
`;

const ActionsContainer = styled.ul`
    
`;

const Action = styled.li`
    font-weight: 700;
	font-size: 1.5rem;
	padding: .6rem 2.4rem;
	border-top: .1rem solid rgba(0,0,0,.15);
`;

const StyledLink = styled(NavLink)`
    display: block;
`;

class Profile extends Component {

    handleClickOutside() {
        if(!this.props.isProfileClosed) {
            this.props.toggleProfile();
        }
    }

	render() {
        const { onLogOutClick, picture, name,  email} = this.props;
        return (
            <ProfileContainer>
                <InfoContainer>
                    <ProfileImage src={picture}/>
                    <Wrapper>
                        <Title3>{name}</Title3>
                        <EmailText>{email}</EmailText>
                    </Wrapper>
                </InfoContainer>
                <ActionsContainer>
                    <Action>
                        <StyledLink to={'/settings'}>Settings</StyledLink>
                    </Action>
                    <Action>
                        <SignOutButton onClick={() =>  onLogOutClick()}>Sign Out</SignOutButton>
                    </Action>
                </ActionsContainer>
            </ProfileContainer>
        )
    }
}

function mapStateToProps(state) {

    return {
        isProfileClosed: state.isProfileClosed
    }
}

function mapDispatchToProps(dispatch) {

    return {
        toggleProfile: () => dispatch(toggleProfile()),
    }
}

export default compose(connect(mapStateToProps, mapDispatchToProps), onClickOutside)(Profile);