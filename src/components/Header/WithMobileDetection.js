import React, { Component } from 'react';

export const WithMobileDetection = (WrappedComponent) => {
    class WithMobileDetection extends Component {
        constructor() {
            super();
            this.state = {
                width: window.innerWidth,
            };
        }

        componentWillMount() {
            window.addEventListener('resize', this.handleWindowSizeChange);
        }

        componentWillUnmount() {
            window.removeEventListener('resize', this.handleWindowSizeChange);
        }

        handleWindowSizeChange = () => {
            this.setState({width: window.innerWidth});
        };

        render() {
            const {width} = this.state;
            const isMobile = width <= 767;

            return <WrappedComponent { ...this.props } isMobile={isMobile}/>;
        }
    }

    return WithMobileDetection;
};