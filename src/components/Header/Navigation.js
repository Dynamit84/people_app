import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const StyledNavigation = styled.nav`
	display: inline-block;
	font-size: 1.5rem;
	line-height: 5rem;
`;

const FlexList = styled.ul`
	display: flex;
	padding-left: 4rem;
`;

const StyledNavLink = styled(NavLink)`
	margin-left: 1rem;
`;

export const Navigation = () => {
	
	return (
        <StyledNavigation>
            <FlexList>
                <li><NavLink to={'/home'} activeClassName={'active'}>Home</NavLink></li>
                <li><StyledNavLink to={'/connections'} activeClassName={'active'}>My Connections</StyledNavLink></li>
            </FlexList>
        </StyledNavigation>
	)
	
};