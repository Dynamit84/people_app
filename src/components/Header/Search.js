import React from 'react';
import styled from 'styled-components';
import { SearchInput } from './SearchInput';

const SearchContainer = styled.div`
	flex-grow: 2;
	display: inline-flex;
	flex-direction: row-reverse;
	height: 2.5rem;
`;

const Input = styled(SearchInput)`
	height: 100%;
	box-sizing: border-box;
	width: 20rem;
	margin-right: 1rem;
	transition: box-shadow 0.30s ease-in-out, border 0.30s ease-in-out;
	outline: none;
	border: 0.1rem solid #DDDDDD;
	padding: 0 1rem;
	border-radius: 0.4rem;
	font-size: 1.2rem;
`;

export const Search = (props) => {
	
	return (
		<SearchContainer>
			<Input {...props} />
		</SearchContainer>
	);
};