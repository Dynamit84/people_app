import React from 'react';

export const SearchInput = ({ className, onSearchChange, autoFocus }) => (
    <input placeholder='Search for people'
           className={className}
           onChange={(event) => onSearchChange(event.target.value)}
           autoFocus={autoFocus}/>
);