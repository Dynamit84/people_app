import React, { Component } from 'react';
import styled from 'styled-components';
import {appear} from "../../../utils/animations";
import { SearchInput } from '../SearchInput';

/*const leave = keyframes`
	0% {
    	transform: translateY(10rem);
	}
	100% {
    	transform: translateY(-1rem);
    	opacity: 0;
	}
`;*/

const SearchContainer = styled.div`
    position: absolute;
    height: 5rem;
    top: 4.4rem;
    width: 100%;
    z-index: 100;
    background: #0073b1 linear-gradient(to right,#0073b1 0,#0084bf 100%);
    padding: .8rem 1.8rem;
    box-sizing: border-box;
    animation: ${appear} 0.4s ease-in-out forwards;
    opacity: 0;
    
    &.search-enter.search-enter-active {
	  	animation: ${appear} .4s ease-in-out forwards;
	}
`;

const MobileInput = styled(SearchInput)`
    width: 100%;
    border: 0;
    border-radius: .2rem;
    background-color: rgba(0,0,0,.2);
    color: #fff;
    box-sizing: border-box;
    height: 3.4rem;
    line-height: 3.4rem;
    font-size: 1.6rem;
    padding: 0 1.5rem;
    
    ::placeholder {
        color: #fff;
        font-size: 1.4rem;
    }
    
    &:focus {
        outline: 0;
        background-color: #fff;
        color: rgba(0,0,0,.55);
        font-weight: 400;
    }
`;

class MobileSearch extends Component{

    render() {
        return (
            <SearchContainer>
                <MobileInput {...this.props} autoFocus={true}/>
            </SearchContainer>
        );
    }
}

export default MobileSearch;