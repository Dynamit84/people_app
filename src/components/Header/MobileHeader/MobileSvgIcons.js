import React from 'react';

export const ConnectionsIcon = () => {
	return <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" className="nav-icon" focusable="false" xmlns="http://www.w3.org/2000/svg"><g className="active-item" style={{fillOpacity: 1}}><path d="M16,17.85V20a1,1,0,0,1-1,1H1a1,1,0,0,1-1-1V17.85a4,4,0,0,1,2.55-3.73l2.95-1.2V11.71l-0.73-1.3A6,6,0,0,1,4,7.47V6a4,4,0,0,1,4.39-4A4.12,4.12,0,0,1,12,6.21V7.47a6,6,0,0,1-.77,2.94l-0.73,1.3v1.21l2.95,1.2A4,4,0,0,1,16,17.85Zm4.75-3.65L19,13.53v-1a6,6,0,0,0,1-3.31V9a3,3,0,0,0-6,0V9.18a6,6,0,0,0,.61,2.58A3.61,3.61,0,0,0,16,13a3.62,3.62,0,0,1,2,3.24V21h4a1,1,0,0,0,1-1V17.47A3.5,3.5,0,0,0,20.75,14.2Z"></path></g><g className="inactive-item" style={{fill: 'currentColor'}}><path d="M20.74,14.2L19,13.54V12.86l0.25-.41A5,5,0,0,0,20,9.82V9a3,3,0,0,0-6,0V9.82a5,5,0,0,0,.75,2.63L15,12.86v0.68l-1,.37a4,4,0,0,0-.58-0.28l-2.45-1V10.83A8,8,0,0,0,12,7V6A4,4,0,0,0,4,6V7a8,8,0,0,0,1,3.86v1.84l-2.45,1A4,4,0,0,0,0,17.35V20a1,1,0,0,0,1,1H22a1,1,0,0,0,1-1V17.47A3.5,3.5,0,0,0,20.74,14.2ZM16,8.75a1,1,0,0,1,2,0v1.44a3,3,0,0,1-.38,1.46l-0.33.6a0.25,0.25,0,0,1-.22.13H16.93a0.25,0.25,0,0,1-.22-0.13l-0.33-.6A3,3,0,0,1,16,10.19V8.75ZM6,5.85a2,2,0,0,1,4,0V7.28a6,6,0,0,1-.71,2.83L9,10.72a1,1,0,0,1-.88.53H7.92A1,1,0,0,1,7,10.72l-0.33-.61A6,6,0,0,1,6,7.28V5.85ZM14,19H2V17.25a2,2,0,0,1,1.26-1.86L7,13.92v-1a3,3,0,0,0,1,.18H8a3,3,0,0,0,1-.18v1l3.72,1.42A2,2,0,0,1,14,17.21V19Zm7,0H16V17.35a4,4,0,0,0-.55-2l1.05-.4V14.07a2,2,0,0,0,.4.05h0.2a2,2,0,0,0,.4-0.05v0.88l2.53,1a1.5,1.5,0,0,1,1,1.4V19Z"></path></g></svg>
};

export const HomeIcon = () => {
	return <svg viewBox="0 0 24 24" width="24px" height="24px" x="0" y="0" preserveAspectRatio="xMinYMin meet" className="nav-icon" focusable="false" xmlns="http://www.w3.org/2000/svg"><g className="active-item" style={{fillOpacity: 1}}><path d="M22,8.45L12.85,2.26a1.5,1.5,0,0,0-1.69,0L2,8.45,3.06,10,4,9.37V19a1,1,0,0,0,1,1h5V15h4v5h5a1,1,0,0,0,1-1V9.37L20.94,10Z"></path></g><g className="inactive-item" style={{fill: 'currentColor'}}><path d="M22,9.45L12.85,3.26a1.5,1.5,0,0,0-1.69,0L2,9.45,3.06,11,4,10.37V20a1,1,0,0,0,1,1h6V16h2v5h6a1,1,0,0,0,1-1V10.37L20.94,11ZM18,19H15V15a1,1,0,0,0-1-1H10a1,1,0,0,0-1,1v4H6V8.89l6-4,6,4V19Z"></path></g></svg>
};

export const SearchIcon = ({isSearchClosed}) => {
	if(isSearchClosed) {
		return (
			<svg width="32" height="32" xmlns="http://www.w3.org/2000/svg">
				<title/>
				<desc/>
				
				<g>
					<title>background</title>
					<rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1"/>
				</g>
				<g>
					<title>Layer 1</title>
					<path fill="#ffffff" id="search" d="m19.42712,21.42712c-1.38987,0.99036 -3.09047,1.57288 -4.92712,1.57288c-4.69442,0 -8.5,-3.80558 -8.5,-8.5c0,-4.69442 3.80558,-8.5 8.5,-8.5c4.69442,0 8.5,3.80558 8.5,8.5c0,1.83665 -0.58252,3.53725 -1.57288,4.92712l5.5848,5.5848c0.5502,0.5502 0.54561,1.43055 -0.0002,1.97636l-0.02344,0.02344c-0.54442,0.54442 -1.43066,0.5459 -1.97636,0.0002l-5.5848,-5.5848l0,0zm-4.92712,-0.42712c3.58985,0 6.5,-2.91015 6.5,-6.5c0,-3.58985 -2.91015,-6.5 -6.5,-6.5c-3.58985,0 -6.5,2.91015 -6.5,6.5c0,3.58985 2.91015,6.5 6.5,6.5l0,0z"/>
					<path id="svg_5" d="m4.472695,12.893533c7.650639,7.239866 -12.990682,-4.621191 -12.944879,-4.626736c-0.045803,0.005545 -16.322665,-1.12408 -16.276862,-1.129624c-0.045803,0.005545 39.491054,9.812739 39.48551,9.807194c0.005544,0.005545 -23.819263,-3.126596 -23.87061,-3.126596c-0.051347,0 14.736465,6.366974 15.198584,9.550462"  style={{opacity:0.5, fillOpacity:null, strokeOpacity: null, strokeWidth: 'NaN', stroke: null, fill: 'none'}}/>
				</g>
			</svg>
		);
	}
	return (
		<svg width="32" height="32" xmlns="http://www.w3.org/2000/svg">
			<title/>
			<desc/>
			
			<g>
				<title>background</title>
				<rect fill="none" id="canvas_background" height="402" width="582" y="-1" x="-1"/>
			</g>
			<g>
				<title>Layer 1</title>
				<path fill="#ffffff" id="search" d="m19.42712,21.42712c-1.38987,0.99036 -3.09047,1.57288 -4.92712,1.57288c-4.69442,0 -8.5,-3.80558 -8.5,-8.5c0,-4.69442 3.80558,-8.5 8.5,-8.5c4.69442,0 8.5,3.80558 8.5,8.5c0,1.83665 -0.58252,3.53725 -1.57288,4.92712l5.5848,5.5848c0.5502,0.5502 0.54561,1.43055 -0.0002,1.97636l-0.02344,0.02344c-0.54442,0.54442 -1.43066,0.5459 -1.97636,0.0002l-5.5848,-5.5848l0,0zm-4.92712,-0.42712c3.58985,0 6.5,-2.91015 6.5,-6.5c0,-3.58985 -2.91015,-6.5 -6.5,-6.5c-3.58985,0 -6.5,2.91015 -6.5,6.5c0,3.58985 2.91015,6.5 6.5,6.5l0,0z"/>
				<path id="svg_5" d="m4.472695,12.893533c7.650639,7.239866 -12.990682,-4.621191 -12.944879,-4.626736c-0.045803,0.005545 -16.322665,-1.12408 -16.276862,-1.129624c-0.045803,0.005545 39.491054,9.812739 39.48551,9.807194c0.005544,0.005545 -23.819263,-3.126596 -23.87061,-3.126596c-0.051347,0 14.736465,6.366974 15.198584,9.550462" style={{opacity:0.5, fillOpacity:null, strokeOpacity: null, strokeWidth: 'NaN', stroke: null, fill: 'none'}} />
				<ellipse stroke="null" ry="6.80342" rx="7.573619" id="svg_11" cy="14.66499" cx="13.997483" style={{strokeOpacity: null, strokeWidth: null, fill: "#ffffff"}}/>
				<ellipse ry="0.128366" rx="2.233576" id="svg_12" cy="16.256733" cx="15.794614" style={{strokeOpacity: null, strokeWidth:null, stroke: null, fill: "#ffffff"}}/>
				<ellipse ry="0.128366" rx="1.206644" id="svg_13" cy="11.635542" cx="13.073246" style={{strokeOpacity: null, strokeWidth:null, stroke: null, fill: "#ffffff"}}/>
			</g>
		</svg>
	);
};

export const BlacklistIcon = () => {
	return (
        <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg">

            <title/>
            <g>
                <title>background</title>
                <rect fill="none" id="canvas_background" height="24" width="24" y="-1" x="-1"/>
            </g>
            <g>
                <title>Layer 1</title>
                <circle stroke="#ffffff" strokeWidth="2px" strokeLinejoin="round" fill="none" id="svg_1" r="4" cy="6.5" cx="11.305202" className="cls-1"/>
                <path stroke="#ffffff" strokeWidth="2px" strokeLinejoin="round" fill="none" id="svg_2" d="m17.649199,12.387301a8.36927,7.160872 0 0 0 -14.113632,5.179868l0,3.254942l7.673968,0.056078" className="cls-1"/>
                <circle stroke="#ffffff" strokeWidth="2px" strokeLinejoin="round" fill="none" id="svg_3" r="3.277138" cy="17.622585" cx="16.939095" className="cls-1"/>
                <line stroke="#ffffff" strokeWidth="2px" strokeLinejoin="round" fill="none" id="svg_4" y2="14.893196" y1="20.825762" x2="18.591695" x1="14.597348" className="cls-1"/>
                <ellipse id="svg_9" cy="6.718084" cx="15.504677" fillOpacity="null" strokeOpacity="null" strokeWidth="2px" stroke="#ffffff" fill="none"/>
                <ellipse id="svg_10" cy="9.000876" cx="16.839895" fillOpacity="null" strokeOpacity="null" strokeWidth="2px" stroke="#ffffff" fill="none"/>
            </g>
        </svg>
	);
};

export const NextPageIcon = () => {
	return <svg enableBackground="new 0 0 32 32" height="32px" id="Слой_1" version="1.1" viewBox="0 0 32 32" width="32px"><path clipRule="evenodd" d="M21.698,15.286l-9.002-8.999  c-0.395-0.394-1.035-0.394-1.431,0c-0.395,0.394-0.395,1.034,0,1.428L19.553,16l-8.287,8.285c-0.395,0.394-0.395,1.034,0,1.429  c0.395,0.394,1.036,0.394,1.431,0l9.002-8.999C22.088,16.325,22.088,15.675,21.698,15.286z" fill="#337ab7" fillRule="evenodd" id="Chevron_Right"/><g/><g/><g/><g/><g/><g/></svg>
};

export const LastPageIcon = () => {
	return <svg enableBackground="new 0 0 32 32" height="32px" id="Слой_1" version="1.1" viewBox="0 0 32 32" width="32px"><g id="Last_Page"><path d="M9.822,6.302c-0.391-0.394-1.024-0.394-1.414,0c-0.391,0.394-0.391,1.032,0,1.426L16.601,16l-8.193,8.272   c-0.391,0.394-0.391,1.032,0,1.426s1.024,0.394,1.414,0l8.899-8.985c0.215-0.215,0.301-0.492,0.279-0.787   c-0.017-0.229-0.115-0.475-0.279-0.64L9.822,6.302z" fill="#337ab7"/><path d="M23.016,6.003c-0.552,0-1,0.448-1,1v17.969c0,0.552,0.448,1,1,1s1-0.448,1-1V7.003   C24.016,6.451,23.568,6.003,23.016,6.003z" fill="#337ab7"/></g><g/><g/><g/><g/><g/><g/></svg>
};