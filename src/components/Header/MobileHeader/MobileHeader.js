import React, { Fragment } from 'react';
import styled, { keyframes } from 'styled-components';
import { NavLink } from 'react-router-dom';
import { HomeIcon, ConnectionsIcon, SearchIcon, BlacklistIcon } from "./MobileSvgIcons";
import { Logo } from "../Header";
import { ProfileIntro } from "../ProfileIntro";
import { TransparentButton } from "../../StyledComponents/Buttons";
import MobileSearch from "./MobileSearch";
import Profile from "../Profile";
import { Overlay } from "../../Overlay/Overlay";
import {shouldRender, USERS_REG_EXP} from "../../../utils/shouldRender";

const activeClassName = 'nav-item-active';

const appear = keyframes`
	0% {
    	transform: translateY(-3rem);
	}
	100% {
		opacity: 1;
    	transform: translateY(0);
	}
`;

const NavItem = styled(NavLink).attrs({
	activeClassName
})`

	display: flex;
    height: 4.4rem;
    box-sizing: border-box;
    align-items: center;
    justify-content: center;
	position: relative;
    
    &:after {
			display: block;
			content: '';
			position: absolute;
			width: 100%;
			bottom: 0;
			border-bottom: 2px solid #fff;
			transform: scaleX(0);
			transition: transform .2s ease-in-out
	}
 
	
	&:active, &:visited {
		color: #fff;
	}
	
	& .active-item {
	  	visibility: hidden;
	  	fill: currentColor;
	}
	
	& .inactive-item {
	  	visibility: visible;
	}
	
	&.${activeClassName}{
		&:after {
			transform: scaleX(1);
	}

	&.${activeClassName} .active-item {
	  	visibility: visible;
	}
	
	&.${activeClassName} .inactive-item {
	  	visibility: hidden;
	}
	
	&.${activeClassName} #svg_1, &.${activeClassName} #svg_2 {
	  	fill: #fff;
	}
`;

const StyledMobileHeader = styled.header`
	position: fixed;
	display: flex;
	opacity: 0;
	height: 4.4rem;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 100;
    background: #0073b1 linear-gradient(to right,#0073b1 0,#0084bf 100%);
    animation: ${appear} 334ms cubic-bezier(0,0,.2,1) .2s forwards;
`;

const MobileNav = styled.nav`
	width: 100%;
`;

const List = styled.ul`
	display: flex;
	align-items: center;
`;

const ListItem = styled.li`
	width: 20%;
`;

const OverlayContent = styled.div`
	position: absolute;
	top: 0;
	bottom: 0;
	width: 100%;
	z-index: 99;
	background-color: rgba(0,0,0,.5);
`;

export const MobileHeader = ({
		isAuthenticated,
		profile,
		handleSearchClick,
		isSearchClosed,
		onProfileClick,
		isProfileClosed,
	 	onLogOutClick,
	    onSearchChange,
	}) => {
		
    return (
		<StyledMobileHeader>
			{
				!isAuthenticated && (
					<Logo>infriends</Logo>
				)
			}
			{
				isAuthenticated && (
					<Fragment>
						<MobileNav>
							<List>
								<ListItem>
									<NavItem to={'/home'} activeClassName={activeClassName}>
										<HomeIcon/>
									</NavItem>
								</ListItem>
								<ListItem>
									<NavItem to={'/connections'} activeClassName={activeClassName}>
										<ConnectionsIcon/>
									</NavItem>
								</ListItem>
                                <ListItem>
                                    <NavItem to={'/blacklist'} activeClassName={activeClassName}>
                                        <BlacklistIcon/>
                                    </NavItem>
                                </ListItem>
							</List>
						</MobileNav>
						{
                            shouldRender(USERS_REG_EXP) &&
							<Fragment>
                                <TransparentButton onClick={() => handleSearchClick()}>
                                    <SearchIcon isSearchClosed={isSearchClosed}/>
                                </TransparentButton>
                                {
                                    !isSearchClosed && <MobileSearch onSearchChange={(val) => onSearchChange(val)}/>
                                }
                            </Fragment>
						}
                        <ProfileIntro onProfileClick={onProfileClick}
                                      picture={profile.picture}/>
						{
							!isProfileClosed && (
								<Fragment>
									<Profile onLogOutClick={onLogOutClick} {...profile}/>
									<Overlay>
										<OverlayContent className={'ignore-react-onclickoutside'} />
									</Overlay>
								</Fragment>
							)
						}
					</Fragment>
				)
			}
		</StyledMobileHeader>
	);
};