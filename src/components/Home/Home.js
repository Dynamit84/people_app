import React, { Component, Fragment } from 'react';
import UsersList from '../Users/UsersList';
import Aside from '../Aside/Aside';
import { Main } from '../StyledComponents/Main';
import { connect } from 'react-redux';
import { LoaderHOC } from "../Loader/LoaderHOC";
import { compose } from 'recompose';
import Pagination from "../Pagination/Pagination";

class Home extends Component {

    render() {
    	const { users, pager } = this.props;

        return (
			<Fragment>
                <Aside users={ users }/>
                    <Main>
						<Pagination/>
                        <UsersList users={ users }/>
                        <Pagination/>
                    </Main>
			</Fragment>
		)
    }
}

const mapStateToProps = (state) => {
	
	return {
		users: state.users,
		isLoading: state.isLoading
	}
};

export default compose(connect(mapStateToProps), LoaderHOC('users'))(Home);