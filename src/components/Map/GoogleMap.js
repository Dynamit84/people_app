import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import { compose, withProps, lifecycle } from 'recompose';
import React from 'react';
import marker from '../../assets/img/location.png';
import styled from 'styled-components';
import { device } from "../../utils/mediaConst";

const MapContainer = styled.div`
	height: 400px;
	
	@media ${device.tablet} {
		width: 50%;
	}
`;

export const MapWithMarker = compose(
	withProps({
		loadingElement: <div style={{ height: `100%` }} />,
		containerElement: <MapContainer />,
		mapElement: <div style={{ height: `100%` }}/>
	}),
	lifecycle({
		componentWillMount() {
			const { nat, location } = this.props;
			const googleMaps = window.google.maps;
			const geocoder = new googleMaps.Geocoder();

			geocoder.geocode({
				'address': location.street,
				componentRestrictions: {
					country: nat,
				}
			}, function(results, status) {
				if (status === 'OK') {
					const { location } = results[0].geometry;
                    this.setState({
                        position: {
                            lat: location.lat(),
                            lng: location.lng()
                        },
						icon: {
                        	url: marker,
							scaledSize: new googleMaps.Size(40, 40),
						}
					});
				} else {
					alert('Geocode was not successful for the following reason: ' + status);
				}
			}.bind(this));
		}
	}),
	withGoogleMap
)(props =>	<GoogleMap defaultZoom={12} center={props.position}>
            	<Marker position={props.position} icon={props.icon}/>
        	</GoogleMap>
);