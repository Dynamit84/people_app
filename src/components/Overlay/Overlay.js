import React, { Component } from  'react';
import ReactDOM from 'react-dom';

const root = document.getElementById('root');

export class Overlay extends Component {
    constructor() {
        super();
        this.el = document.createElement('div');
    }

    componentDidMount() {
        root.appendChild(this.el);
    }

    componentWillUnmount() {
        root.removeChild(this.el);
    }

    render() {

        return ReactDOM.createPortal(
            this.props.children,
            this.el
        );
    }
}
