import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { push } from 'react-router-redux';

export const WithoutAuthentication = (WrappedComponent) => {
	class WithoutAuthentication extends Component {
		componentWillMount() {
			if (this.props.isAuthenticated) {
				this.props.push('/home');
			}
		}
		
		componentWillUpdate(nextProps) {
			if (nextProps.isAuthenticated) {
				this.props.push('/home');
			}
		}
		
		render() {
			
			return this.props.isAuthenticated ? <Redirect to={'/home'}/> : <WrappedComponent {...this.props} />;
		}
	}
	
	function mapStateToProps(state) {
		return {
			isAuthenticated: state.auth.isAuthenticated
		};
	}
	
	const mapDispatchToProps = (dispatch) => {
		
		return {
			push: path => dispatch(push(path))
		}
	};
	
	return connect(mapStateToProps, mapDispatchToProps)(WithoutAuthentication);
};