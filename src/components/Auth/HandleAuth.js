import React, { Component } from 'react';
import { connect } from "react-redux";
import { loginError, loginSuccess } from "../../actions/actions";
import { push } from 'react-router-redux';
import { Redirect} from 'react-router-dom';

class HandleAuth extends Component {
	
	componentWillMount() {
		this.handleAuthentication();
	}
	
	handleAuthentication() {
		const { auth0 } = this.props;
		auth0.parseHash((err, authResult) => {
			if (authResult && authResult.accessToken && authResult.idToken) {
				this.setSession(authResult);
				this.getProfile();
			}
		});
	}
	
	getProfile() {
		const { auth0, push, loginSuccess, loginError } = this.props;
		const accessToken = this.getAccessToken();
		auth0.client.userInfo(accessToken, (err, profile) => {
			if (!profile) {
				loginError(err);
			} else {
				loginSuccess(profile);
				localStorage['profile'] = JSON.stringify(profile);
				push('/home');
			}
		});
	}
	
	getAccessToken() {
		const accessToken = localStorage['access_token'];
		if (!accessToken) {
			throw new Error('No access token found');
		}
		return accessToken;
	}
	
	setSession(authResult) {
		// Set the time that the access token will expire at
		const { expiresIn, accessToken, idToken } = authResult;
		const expiresAt = JSON.stringify((expiresIn * 1000) + new Date().getTime());
		localStorage['access_token'] = accessToken;
		localStorage['id_token'] = idToken;
		localStorage['expires_at'] = expiresAt;
	}
	
	render() {
		
		return null;
	}
}

const mapStateToProps = (state) => {

    return {
        auth0: state.auth.auth0
    }
};

const mapDispatchToProps = (dispatch) => {
	
	return {
		loginError: error => dispatch(loginError(error)),
		loginSuccess: profile => dispatch(loginSuccess(profile)),
        push: path => dispatch(push(path))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(HandleAuth);