import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';

export const WithAuthentication = (config) => (WrappedComponent) => {
	class WithAuthentication extends Component {
		componentWillMount() {
			if (!this.props.isAuthenticated) {
				this.props.history.push('/login');
			}
		}
		
		componentWillUpdate(nextProps) {
			if (!nextProps.isAuthenticated) {
				this.props.history.push('/login');
			}
		}
		
		PropTypes = {
			router: PropTypes.object,
		};
		
		render() {
			return !this.props.isAuthenticated ? <Redirect to={'/login'}/> : <WrappedComponent config={config} {...this.props} />;
		}
	}
	
	function mapStateToProps(state) {
		return {
			isAuthenticated: state.auth.isAuthenticated
		};
	}
	
	return connect(mapStateToProps)(WithAuthentication);
};