import React from 'react';
import styled from 'styled-components';
import { Image } from "../StyledComponents/Image";
import {FlexWrapper} from "../Header/Header";
import facebook from '../../assets/img/socials/facebook.png';
import google from '../../assets/img/socials/google.png';
import instagram from '../../assets/img/socials/instagram.png';
import twitter from '../../assets/img/socials/twitter.png';
import youtube from '../../assets/img/socials/youtube.png';
import {device} from "../../utils/mediaConst";

const SOCIALS = [
    {
    	link: 'https://www.facebook.com/',
        image: facebook
    },
    {
        link: 'https://plus.google.com/',
        image: google
    },
    {
		link: 'https://www.instagram.com/',
        image: instagram
	},
    {
        link: 'https://twitter.com/',
        image: twitter
	},
	{
        link: 'https://www.youtube.com/',
        image: youtube
    }
];

const StyledFooter = styled.footer`
	padding: 1rem;
	background: #1bbc9b;
	border-top: .4rem solid #555e67;
	text-align: center;
	color: #fff;
	
	@media ${device.tablet} {
		display: flex;
        align-items: center;
        justify-content: space-between;
        height: 4rem;
        flex-flow: row-reverse; 
        padding: 1rem 6rem;
	}
`;

const StyledImage = styled(Image)`
	width: 3.5rem;
	height: 3.5rem;
`;

const Text14 = styled.p`
	font-size: 1.4rem;
`;

const SocialLink = styled.a`
	margin-left: .7rem;
`;

const StyledFlexWrapper = styled(FlexWrapper)`
      justify-content: center;
      margin-bottom: .5rem;
`;

export const Footer = () => {
	
	return (
		<StyledFooter>
			<StyledFlexWrapper>
				<Text14>Follow Us On</Text14>
				{
					SOCIALS.map((social, index) => <SocialLink href={social.link} key={index}><StyledImage src={social.image} /></SocialLink>)
				}
			</StyledFlexWrapper>
            <p>Copyright &copy; 2018 INFRIENDS All rights reserved</p>
		</StyledFooter>
	);
};