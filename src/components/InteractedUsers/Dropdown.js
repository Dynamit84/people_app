import React from 'react';
import styled from 'styled-components';
import { SortButton, CurrentFilter } from "./UsersActions";

const StyledDropdown = styled.ul`
    background-clip: padding-box;
    background-color: #fff;
    box-shadow: 0 0 0 .1rem rgba(0,0,0,.1), 0 .6rem .9rem rgba(0,0,0,.2);
    border-radius: 0 0 .2rem .2rem;
    position: absolute;
    padding: .8rem;
    width: auto;
    z-index: 99;
    font-size: 1.4rem;
    text-transform: capitalize;
    top: 2.6rem;
    right: -5rem;
    
    &:before {
		border: solid transparent;
		border-width: 0 1rem;
		border-bottom: 1rem #cdcfd2 solid;
		content: "";
		height: 0;
		width: 0;
		margin-left: -6.5rem;
		top: -1rem;
    }
    
    &:after {
		border: solid transparent;
		border-width: 0 .9rem;
		border-bottom: .9rem #fff solid;
		content: "";
		height: 0;
		width: 0;
		margin-left: -6.4rem;
		top: -.9rem;
		
    }
    
    &:after, &:before {
    	position: absolute;
		left: 100%;
    }
`;

const SortItem = styled.li`
    padding: .3rem 1rem;
    
	&:hover {
		background-color: #cdcfd2;
	}
`;

export const Dropdown = ({ sort, isDropdownClosed, onSortClick }) => {
	
	if(isDropdownClosed) {
		
		return null;
	}
	
	return (
		<StyledDropdown>
			{
				sort.map((filter, index) => {
					
					return (
						<SortItem key={index}>
							<SortButton onClick={() => onSortClick(filter)}>
								<CurrentFilter>{filter}</CurrentFilter>
							</SortButton>
						</SortItem>
					)
				})
			}
		</StyledDropdown>
	);
};