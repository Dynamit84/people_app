import React, { Component } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Dropdown } from "./Dropdown";
import { toggleDropdown, sortList, clearConnections } from "../../actions/actions";
import onClickOutside from 'react-onclickoutside';
import { compose } from 'recompose';
import { Button } from "../StyledComponents/Buttons";
import { device } from "../../utils/mediaConst";

const fontSize = 1.5;
const sortItems = ['recently added', 'first name', 'last name'];

const ActionsContainer = styled.div`
  	color: rgba(0,0,0,.55);
	margin-bottom: 1.5rem;
	
	@media ${device.phoneL} {
		display: flex;
		justify-content: space-between;
		align-items: center;
	}
`;

export const SortButton = styled.button`
	background: 0 0;
	border: none;
	padding: 0;
	cursor: pointer;
    outline: none;
    color: rgba(0,0,0,.55);
	width: 100%;
    text-align: left;
`;

const OpenSortButton = styled(SortButton)`
    background-color: #fff;
    box-shadow: none;
    font-weight: 600;
    display: flex;
    align-items: center;
`;

const Text = styled.p`
    font-weight: 400;
    font-size: ${fontSize}rem;
    margin-right: 1.5rem;
`;

const ArrowDown = styled.i`
	border: solid rgba(0,0,0,.55);
    border-width: 0 .2rem .2rem 0;
    display: inline-block;
    padding: .3rem;
    transform: rotate(45deg);
    margin-left: .7rem;
`;

export const CurrentFilter = styled.span`
	text-transform: capitalize;
	font-size: ${fontSize}rem;
`;

const DropdownContainer = styled.div`
	position: relative;
`;

const SortContainer = styled.div`
	display: flex;
`;

const ButtonWrapper = styled.div`
	width: 100%;
	margin-bottom: 1.5rem;
	
	@media ${device.phoneL} {
		width: auto;
    	order: 2;
    	margin: 0;
	}
`;

const CapitalizedSpan = styled.span`
	text-transform: capitalize;
`;

class ConnectionActions extends Component {
	
	handleClickOutside() {
		if(!this.props.isSortClosed) {
			this.props.toggleDropdown();
		}
	}
	
	handleSortClick(sortItem) {
		const {sortList, sortBy, toggleDropdown} = this.props;
		
		if(sortItem !== sortBy) {
			sortList(sortItem);
		}
        toggleDropdown();
	}
	
	handleClearClick() {
		const { clearConnections, users } = this.props;
		//localStorage.removeItem('connections');
		clearConnections(users);
	}

	handleSearchChange(e) {
		this.props.onInputChange(e.target.value.toLowerCase());
	}

    render() {
    	const { isSortClosed, sortBy, toggleDropdown, btnCaption } = this.props;
    	
        return (
            <ActionsContainer>
                <ButtonWrapper>
                    <Button style={{width: '13.5rem'}} onClick={() => this.handleClearClick()}>
                        <CapitalizedSpan>clear {btnCaption}</CapitalizedSpan>
                    </Button>
				</ButtonWrapper>
                <SortContainer>
                    <Text>Sort by:</Text>
                    <DropdownContainer>
                        <OpenSortButton onClick={() => toggleDropdown()}>
                            <CurrentFilter>{sortBy}</CurrentFilter>
                            <ArrowDown/>
                        </OpenSortButton>
                        <Dropdown sort={sortItems} isDropdownClosed={isSortClosed} onSortClick={this.handleSortClick.bind(this)}/>
                    </DropdownContainer>
				</SortContainer>
            </ActionsContainer>
        );
    }
}

const mapStateToProps = (state) => {

    return {
		users: state.users,
        sortBy: state.sortBy,
		isSortClosed: state.isSortClosed
    }
};

const mapDispatchToProps = (dispatch) => {
	
	return {
		toggleDropdown: () => dispatch(toggleDropdown()),
		sortList: (sortItem) => dispatch(sortList(sortItem)),
		clearConnections: (users) => dispatch(clearConnections(users))
	}
};

export default compose(connect(mapStateToProps, mapDispatchToProps), onClickOutside)(ConnectionActions);