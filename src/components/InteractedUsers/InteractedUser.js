import React from 'react';
import styled from 'styled-components';
import { Image } from '../StyledComponents/Image';
import ViewButton, { RemoveButton } from '../StyledComponents/Buttons';
import { NavLink } from 'react-router-dom';
import  { calculateConnectionDate } from '../../utils/connectionDate';
import { device } from "../../utils/mediaConst";
import {leave} from "../../utils/animations";

const ConnectionContainer = styled.li`
  display: flex;
  align-items: center;
  border-bottom: 1px solid #e6e9ec;
  position: relative;
  padding: .5rem 0; 
  height: 9rem;
  box-sizing: border-box;
  
  &.card-leave.card-leave-active {
	  	animation: ${leave} .6s cubic-bezier(.55,-0.04,.91,.94) forwards;
	}
  
  &:after {
    content: '';
    width: 9rem;
    height: .2rem;
    background: #fff;
    position: absolute;
    bottom: -.1rem;
    left: 0;
  }
`;

const InfoContainer = styled.div`
  text-align: center;
  width: 100%;
`;

const ConnectionImage = styled(Image)`
	@media ${device.tablet} {
        width: 8.4rem;
        height: 8.4rem;
	}
`;

const StyledNavLink = styled(NavLink)`
    font-size: 1.5rem;
    overflow: hidden;
    text-overflow: ellipsis;
    max-width: 85%;
    white-space: nowrap;
    display: inline-block;
    @media ${device.phoneL} {
        font-size: 2rem;
    }
`;

const ConnectionText = styled.p`
    font-weight: 400;
    color: rgba(0,0,0,.55);
    font-size: 1.3rem;
    @media ${device.phoneL} {
        font-size: 1.5rem;
    }
`;

const ButtonContainer = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    @media ${device.tablet} {
        
    }
`;

export const InteractedUser = ({ picture, name, connectionDate, blacklistedDate, id, onRemoveClick, infoText }) => {
    const formattedConnectionDate = calculateConnectionDate(connectionDate || blacklistedDate);
    const { fullName } = name;

    return (
        <ConnectionContainer>
            <NavLink to={`users/${id.value}`}>
                <ConnectionImage src={picture.large} alt={fullName}/>
            </NavLink>
            <InfoContainer>
                <StyledNavLink to={`users/${id.value}`}>{fullName}</StyledNavLink>
                <ConnectionText> {infoText} {formattedConnectionDate}</ConnectionText>
            </InfoContainer>
            <ButtonContainer>
                <ViewButton id={id.value} bigger="true"/>
                <RemoveButton id={id.value} margin='true' caption={blacklistedDate ? 'whitelist' : 'remove' } onRemoveClick={() => onRemoveClick(id.value, fullName)}/>
            </ButtonContainer>
        </ConnectionContainer>
    );
};

