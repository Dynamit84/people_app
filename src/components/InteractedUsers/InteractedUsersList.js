import React, { Component } from 'react';
import styled from 'styled-components';
import { Main } from "../StyledComponents/Main";
import { connect } from 'react-redux';
import { InteractedUser } from './InteractedUser';
import { removeConnection, removeBlacklisted } from "../../actions/actions";
import ConnectionActions from "./UsersActions";
import { sortName, sortNumbers } from "../../utils/sort";
import { LoaderHOC } from "../Loader/LoaderHOC";
import { compose } from 'recompose';
import { NotificationHOC } from "../Notification/NotificationHOC";
import { startFromCapital } from "../../utils/startFromCapital";
import { CSSTransitionGroup } from 'react-transition-group';

const InteractedUsersList = styled.div`
	border-radius: .2rem;
    background: #fff;
	box-shadow: 0 0 0 .1rem rgba(0,0,0,.1), 0 .2rem .3rem rgba(0,0,0,.2);
	padding: 2rem 2rem;
    box-sizing: border-box;
    margin: 0 auto;
	max-width: 50rem;		
`;

const Title2 = styled.h2`
	font-size: 2.5rem;
    font-weight: 400;
    margin-top: 0;
    text-transform: capitalize;
    color: ${props => props.connection ? '#0073b1' : '#e01414'};
`;

const UsersContainer = styled.ul`
	border-top: 1px solid #e6e9ec;
`;

class MyConnections extends Component {
	constructor(props) {
		const { headerText } = props.config;
		super();
		this.state = {
			[headerText]: this.getUsers(props),
			usersToRender: []
		}
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.searchValue !== this.props.searchValue) {
            this.setState({
                usersToRender: this.getUsers(nextProps),
           	})
		}
	}

    onRemoveClick(id, fullName) {
        const userIndex = this.props.users.findIndex(user => user.id.value === id);
        const { headerText } = this.props.config;
        const removeFrom = `removeFrom${startFromCapital(headerText)}`;
		let users = JSON.parse(localStorage[headerText]);
		users = users.filter(data => data.id !== id);
		localStorage[headerText] = JSON.stringify(users);

        this.props[removeFrom](userIndex);
        this.props.createNotification({
			type: 'success',
			title: `${fullName}`,
			message: `has been successfully removed from your ${headerText === 'connections' ? 'connections list' : 'blacklist'}`
		});
	}

	isUserSearched (user, searchValue) {
		return user.name.fullName.toLowerCase().search(searchValue) !== -1;
	}

    getUsers(props) {
	    const { users, sortBy, config, searchValue } = props;
	    const { infoText, interactionDate } = config;
	    const interacted = users.filter(user => user[infoText]);
        let sortedUsers;

    	switch (sortBy) {
			case ('first name'):
				sortedUsers = sortName(interacted, 'first');
				break;
			case ('last name'):
				sortedUsers = sortName(interacted, 'last');
				break;
			default:
				sortedUsers = sortNumbers(interacted, interactionDate);
		}

		return searchValue ? sortedUsers.filter(user => this.isUserSearched(user, searchValue)): sortedUsers;
	}

	render() {
        const { config } = this.props;
		const { headerText, clearBtnText } = config;
		const { usersToRender } = this.state;
		const users = usersToRender.length ? usersToRender : this.state[headerText];
		const length = this.state[headerText].length;
		let message;
		if(headerText === 'connections') {
            message = length === 1 ? 'connection' : 'connections';
		} else {
			message = headerText;
		}


		return  (
			<Main>
				<InteractedUsersList>
					<Title2 connection={headerText === 'connections'}>{length} {message}</Title2>
					<ConnectionActions btnCaption={clearBtnText} eventTypes="click" />
                    <UsersContainer>
                        <CSSTransitionGroup
                            transitionName="card"
                            transitionEnter={false}
                            transitionLeaveTimeout={400}>
                            {
                                users.map(user => <InteractedUser {...user} infoText={config.infoText} onRemoveClick={this.onRemoveClick.bind(this)} key={user.id.value} />)
                            }
                        </CSSTransitionGroup>
                    </UsersContainer>
				</InteractedUsersList>
			</Main>
		);
	}
}

const mapStateToProps = (state) => {

	return {
		users: state.users,
		isLoading: state.isLoading,
		sortBy: state.sortBy,
		searchValue: state.searchValue
	}
};

const mapDispatchToProps = (dispatch) => {
	
	return {
		removeFromConnections: (id) => dispatch(removeConnection(id)),
		removeFromBlacklisted: (id) => dispatch(removeBlacklisted(id))
	}
};

export default compose(connect(mapStateToProps, mapDispatchToProps), LoaderHOC('users'), NotificationHOC())(MyConnections);