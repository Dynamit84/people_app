import React, { Component } from 'react';
import styled from 'styled-components';
import PageButton from "./PageButton";
import { connect } from 'react-redux';

const PaginationWrapper = styled.div`
	text-align: center;
`;

const PagesContainer = styled.ul`
	display: inline-flex;
	font-size: 1.5rem;
	color: #337ab7;
`;

class Pagination extends Component{

	render() {

        return (
            <PaginationWrapper>
                <PagesContainer>
                    <PageButton isRotated buttonType={'last'}/>
                    <PageButton isRotated buttonType={'next'}/>
                    {this.props.pager.pages.map((page, index) =>
                            <PageButton key={index} page={page}/>
                        /*<PageButtonContainer key={index} className={pager.currentPage === page ? 'active' : ''}>
                            <PageButton onClick={() => this.setPage(++page)}>{++page}</PageButton>
                        </PageButtonContainer>*/
                    )}
                    <PageButton buttonType={'next'}/>
                    <PageButton buttonType={'last'}/>
                    {/*<PageButtonContainer className={pager.currentPage === 1 ? 'disabled' : ''}>
                        <PageButton isRotated={true} onClick={() => this.setPage(1)}>
                            <LastPageIcon/>
                        </PageButton>
                    </PageButtonContainer>
                    <PageButtonContainer className={pager.currentPage === 1 ? 'disabled' : ''}>
                        <PageButton isRotated={true} onClick={() => this.setPage(pager.currentPage - 1)}>
                            <NextPageIcon/>
                        </PageButton>
                    </PageButtonContainer>
                    {pager.pages.map((page, index) =>
                        <PageButtonContainer key={index} className={pager.currentPage === page ? 'active' : ''}>
                            <PageButton onClick={() => this.setPage(++page)}>{++page}</PageButton>
                        </PageButtonContainer>
                    )}
                    <PageButtonContainer className={pager.currentPage === pager.totalPages ? 'disabled' : ''}>
                        <PageButton onClick={() => this.setPage(pager.currentPage + 1)}>
                            <NextPageIcon/>
                        </PageButton>
                    </PageButtonContainer>
                    <PageButtonContainer className={pager.currentPage === pager.totalPages ? 'disabled' : ''}>
                        <PageButton onClick={() => this.setPage(pager.totalPages)}>
                            <LastPageIcon/>
                        </PageButton>
                    </PageButtonContainer>*/}
                </PagesContainer>
            </PaginationWrapper>
        );
	}
}

const mapStateToProps = (state) => {

	return {
		pager: state.pager
	}
};

export default connect(mapStateToProps)(Pagination);