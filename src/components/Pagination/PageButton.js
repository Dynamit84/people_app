import React, { Component } from 'react';
import styled from 'styled-components';
import { LastPageIcon, NextPageIcon } from "../Header/MobileHeader/MobileSvgIcons";
import { connect } from 'react-redux';
import { setPage } from "../../actions/actions";

const PageButtonContainer = styled.li`
	width: 3.2rem;
	background-color: #fff;
	display: flex;
`;

const ChangePageLink = styled.a`
    height: 3.2rem;
    width: 3.2rem;
    line-height: 3.2rem;
    border: #cdcfd2 solid .1rem;
	box-sizing: border-box;
	${props => {
			if (props.buttonStyle === 'disabled') {
				return `
					& svg {
						& path {
							fill: #ddd;
					}
			  	`;
			}
			if (props.buttonStyle === 'active') {
				return `
					z-index: 2;
					color: #fff;
					cursor: default;
					background-color: #337ab7;
					border-color: #337ab7;
				`;
    }
  		}
	}
	//display: inline-block;
	//border: #cdcfd2 solid .1rem;
	
	
	/*:first-of-type {
		border-top-left-radius: .4rem;
    	border-bottom-left-radius: .4rem;
	}
	
	:last-of-type {
		border-top-right-radius: .4rem;
    	border-bottom-right-radius: .4rem;
	}
	*/
	& svg {
		transform: ${props => props.isRotated ? 'rotate(180deg)' : 'none'};
		/*border: .1rem solid;
    	box-sizing: border-box;*/
	}
`;

const ButtonContent = (props) => {
	const { buttonType, page } = props;
	
	if(buttonType) {

		return buttonType === 'last' ? <LastPageIcon/> : <NextPageIcon/>;
	} else {

		return page;
	}
};

class PageButton extends Component{

    changePage(setPage, buttonType, isRotated, pager, page) {
    	let pageNumber;

    	switch(true) {
			case isRotated && buttonType === 'last':
				pageNumber = 1;
				break;

            case isRotated && buttonType === 'next':
                pageNumber = pager.currentPage - 1;
                break;

            case buttonType === 'next':
                pageNumber = pager.currentPage + 1;
                break;

			case buttonType === 'last':
                pageNumber = pager.totalPages;
                break;

			default:
                pageNumber = page;
		}

		setPage(pageNumber);

    }

   render () {
	   const { pager, setPage, isRotated, buttonType, page } = this.props;
	   const { currentPage, totalPages } = pager;
	   let buttonStyle;

	   if(buttonType && (currentPage === 1 || currentPage === totalPages)) {
           buttonStyle = 'disable';
	   } else if(currentPage === page) {
           buttonStyle = 'active';
	   }

	   return (
		   <PageButtonContainer>
			   <ChangePageLink isRotated={isRotated}
							   buttonStyle={buttonStyle}
							   onClick={() => this.changePage(setPage, buttonType, isRotated, pager, page)}>
				   <ButtonContent buttonType={buttonType} page={page}/>
			   </ChangePageLink>
		   </PageButtonContainer>
	   );
   }
}

const mapStateToProps = (state) => {
	
	return {
		pager: state.pager
	}
};

const mapDispatchToProps = (dispatch) => {
	
	return {
		setPage: (pageNumber) => dispatch(setPage(pageNumber))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(PageButton);
