import React, { Component, Fragment } from 'react';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

export const NotificationHOC = () => (WrappedComponent) => {
	return class NotificationHOC extends Component {
		createNotification({type, title, message, timeOut}) {
            NotificationManager[type](message, title, timeOut || 3000);
        }
		
		render() {
			return (
				<Fragment>
					<WrappedComponent createNotification={config => this.createNotification(config)} {...this.props} />
					<NotificationContainer/>
				</Fragment>
			)
		}
	}
};