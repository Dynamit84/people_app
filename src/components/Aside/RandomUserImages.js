import React from 'react';
import styled from 'styled-components';
import { Image } from "../StyledComponents/Image";

const StyledImage = styled(Image)`
    border: .1rem solid #fff;
    width: 3.6rem;
    height: 3.6rem;
    
    &:nth-of-type(1n+2) {
    	margin-left: -1.4rem;
	}
`;

export const RandomUserImages = ({ randomUsers }) => {

	if (randomUsers.length === 0) {

		return null;
	}
	
	return (
				<React.Fragment>
                    {
                        randomUsers.map(user => <StyledImage key={user.id.value} src={user.picture.large} alt=""/>)
                    }
				</React.Fragment>
)
};