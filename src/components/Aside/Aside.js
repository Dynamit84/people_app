import styled from 'styled-components';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { RandomUserImages } from "./RandomUserImages";
import { populateRandomUsers } from '../../utils/generateRandomUsers';
import { device } from "../../utils/mediaConst";

export const StyledAside = styled.aside`
	width: 100%;
    border-radius: .2rem;
	text-align: center;
	background-color: #fff;
	box-shadow: 0 0 0 .1rem rgba(0,0,0,.1), 0 .2rem .3rem rgba(0,0,0,.2);
	padding: 2rem 1rem;
    box-sizing: border-box;
	display: flex;
    align-items: center;
    justify-content: space-around;
    margin: 4rem 0 0;
    
    @media ${device.tablet} {
    	position: fixed;
    	width: 18rem;
    	margin: 12rem 0 0 5rem;
    }
`;

const NumberNavLink = styled(NavLink)`
	font-size: 3.4rem;
	color: #0084bf;
	font-weight: 200;
`;

const Title3 = styled.h3`
	font-size: 1.6rem;
	margin: 0;
`;

const StyledNavLink = styled(NavLink)`
	font-size: 1.3rem;
	display: none;
	font-weight: 600;
	
	 @media ${device.tablet} {
	 	display: inline;
	 }
`;

export default class Aside extends Component {
	constructor() {
		super();
		this.state = {
			randomUsersImages: [],
			connectionsAmount: 0
		}
	}
	
	componentDidMount() {
		const { users } = this.props;
		const connections = users.filter(user => user.connected);
		
		this.setState({
			randomUsersImages: populateRandomUsers(connections),
			connectionsAmount: connections.length
		});
	}
	
	render() {
        const { randomUsersImages, connectionsAmount } = this.state;

		return (
			<StyledAside>
				<NumberNavLink to={'/connections'}>{connectionsAmount}</NumberNavLink>
				<Title3>Your connections</Title3>
				<StyledNavLink to={'/connections'}>See all</StyledNavLink>
				<div>
                    <NumberNavLink to={'/connections'}>
                        <RandomUserImages randomUsers={randomUsersImages}/>
                    </NumberNavLink>
				</div>
			</StyledAside>
		)
	}
};