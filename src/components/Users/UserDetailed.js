import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Image } from "../StyledComponents/Image";
import { MapWithMarker } from "../Map/GoogleMap";
import {addConnection, removeConnection, removeBlacklisted} from "../../actions/actions";
import { Loader } from '../Loader/Loader';
import { Main } from "../StyledComponents/Main";
import background from '../../assets/img/profile-background.svg';
import emailPicture from '../../assets/img/email.png';
import phonePicture from '../../assets/img/phone.png';
import cellPicture from '../../assets/img/cellphone.png';
import dobPicture from '../../assets/img/calendar.png';
import { getAge } from "../../utils/getAge";
import { BiggerButton, RemoveButton } from '../StyledComponents/Buttons';
import { device } from "../../utils/mediaConst";
import {startFromCapital} from "../../utils/startFromCapital";
import {calculateConnectionDate} from "../../utils/connectionDate";

const imageWidth = 14;
const ProfileContainer = styled.div`
	max-width: 80rem;
	margin: 0 auto;
	position: relative;
	text-align: center;
	box-shadow: 0 0 0 1px rgba(0,0,0,.1), 0 2px 3px rgba(0,0,0,.2);
	background-color: #fff;
    border-radius: 2px;
    color: rgba(0,0,0,.7);
`;
const ProfileBackground = styled.div`
	width: 100%;
	position: relative;
	padding-bottom: 40%;
	
	@media ${device.phoneL} {
		padding-bottom: 25%;
	}
`;
const ImageContainer = styled.div`
    width: ${imageWidth}rem;
    height: ${imageWidth}rem;
    border-radius: 50%;
    background-color: rgba(243,246,248,.94);
    border: .5rem solid #fff;
    box-shadow: inset 0 1.5px 3px 0 rgba(0,0,0,.15), 0 1.5px 3px 0 rgba(0,0,0,.15);
	position: relative;
	margin: -${imageWidth/2}rem auto 0;
`;
const StyledImage = styled(Image)`
	width: ${imageWidth}rem;
	height: ${imageWidth}rem;
`;

const Name = styled.p`
	font-size: 3rem;
	margin: 1rem 0;
`;

const InfoWrapper = styled.div`
	display: flex;
	align-items: center;
`;

const Icon = styled.img`
	width: 2.5rem;
	height: 2.5rem;
	margin-right: 1rem;
`;

const Text = styled.p`
	margin: 0;
	font-size: 1.4rem;
`;

const UserInfoContainer = styled.div`
    margin-bottom: 2rem;
    
	@media ${device.tablet} {
		width: 50%;
		margin: 0;
	}
`;

const InfoMapContainer = styled.div`
	padding: 2rem 2rem;
	
	@media ${device.tablet} {
		display: flex;
		padding: 3rem 2rem;
	}
`;

const Age = styled.p`
	font-size: 2rem;
	margin-bottom: 1rem;	
`;

const Title3 = styled.h3`
	font-size: 2.4rem;
	margin: 0 0 1.5rem;
    
    @media ${device.tablet} {
		text-align: left;
	}
    
`;

const InteractionDate = styled.p`
	font-size: 1.5rem;
    margin-bottom: 1rem;
`;

class UserDetailed extends Component {

	saveToLocalStorage(data, storage = 'connections') {
        localStorage[storage] = JSON.stringify(data);
	}

    onConnectClick(id) {
        let myConnections = localStorage['connections'] ? JSON.parse(localStorage['connections']) : [];
		const userIndex = this.props.users.findIndex(user => user.id.value === id);
        const connectionDate = Date.now();
		
		myConnections = [...myConnections, {id, connectionDate}];
        this.saveToLocalStorage(myConnections);
		this.props.addConnection({userIndex, connectionDate});
	}

    onRemoveUser(id, storage) {
        const userIndex = this.props.users.findIndex(user => user.id.value === id);
        let users = JSON.parse(localStorage[storage]);
        const removeFrom = `removeFrom${startFromCapital(storage)}`;
        users = users.filter(data => data.id !== id);
        this.saveToLocalStorage(users, storage);
        this.props[removeFrom](userIndex);
	}

	getProperButton(user) {
		const id = user.id.value;
		let caption, storage;

		switch (true) {
			case user.connected:
                caption = 'remove';
                storage = 'connections';
                break;
			case user.blacklisted:
				caption = 'whitelist';
				storage = 'blacklisted';
				break;
			default:
                return <BiggerButton onClick={() => this.onConnectClick(id)}>Connect</BiggerButton>;
		}

		return <RemoveButton id={id} onRemoveClick={this.onRemoveUser.bind(this, id, storage)} caption={caption}/>;
	}

	getProperInteractionCaption(user) {
		let interaction;
		const {connectionDate, blacklistedDate, connected, blacklisted} = user;
        switch (true) {
            case connected:
                interaction = 'connected';
                break;
            case blacklisted:
                interaction = 'blacklisted';
                break;
            default:
                return null;
        }

        const formattedConnectionDate = calculateConnectionDate(connectionDate || blacklistedDate);

        return <InteractionDate>{interaction} {formattedConnectionDate}</InteractionDate>

	}

    render() {
		const { isLoading, user } = this.props;
		if (isLoading) {
		
			return <Loader/>
		}
		const { picture, name, email, dob, phone, cell, location, nat } = user;

		return (
			<Main>
                <ProfileContainer>
					<ProfileBackground style={{backgroundImage: `url(${background})`, backgroundSize: 'cover'}}/>
					<ImageContainer>
                        <StyledImage src={picture.large}/>
					</ImageContainer>
                    <Name>{name.fullName}</Name>
					<Age>{getAge(dob)} years</Age>
					{
                        this.getProperInteractionCaption(user)
					}
					{this.getProperButton(user)}
					<InfoMapContainer>
						<UserInfoContainer>
							<Title3>Intro</Title3>
							<InfoWrapper>
								<Icon src={emailPicture}/>
								<Text>{email}</Text>
							</InfoWrapper>
							<InfoWrapper>
								<Icon src={dobPicture}/>
								<Text>Date of birth {dob.split(' ')[0]}</Text>
							</InfoWrapper>
							<InfoWrapper>
								<Icon src={phonePicture}/>
								<Text>{phone}</Text>
							</InfoWrapper>
							<InfoWrapper>
								<Icon src={cellPicture}/>
								<Text>{cell}</Text>
							</InfoWrapper>
						</UserInfoContainer>
						<MapWithMarker location={location} nat={nat} />
					</InfoMapContainer>
                </ProfileContainer>
			</Main>
		);
    }
}

const mapStateToProps = (state, ownProps) => {

	return {
		user: state.users.find(user => user.id.value === ownProps.match.params.id),
		isLoading: state.isLoading,
		users: state.users
	}
};

const mapDispatchToProps = (dispatch) => {
	
	return {
		addConnection: userData => dispatch(addConnection(userData)),
        removeFromConnections: (id) => dispatch(removeConnection(id)),
        removeFromBlacklisted: (id) => dispatch(removeBlacklisted(id)),
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(UserDetailed);