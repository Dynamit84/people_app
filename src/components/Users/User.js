import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ViewButton from '../StyledComponents/Buttons';
import { Image } from '../StyledComponents/Image';
import styled from 'styled-components';
import { getAge } from "../../utils/getAge";
import { NavLink } from 'react-router-dom';
import crossImage from '../../assets/img/close.svg';
import { connect } from 'react-redux';
import { addToBlacklist } from "../../actions/actions";
import { device } from "../../utils/mediaConst";
import { leave } from "../../utils/animations";

const ImageNavLink = styled(NavLink)`
	display: inline-block;
	margin: 0;
	
	@media ${device.tablet} {
    	margin: 2rem 0 .5rem;
    }
`;

const StyledNavLink = styled(NavLink)`
	overflow: hidden;
    text-overflow: ellipsis;
    max-width: 90%;
    white-space: nowrap;
    display: inline-block;
`;

const ButtonWrapper = styled.div`
	
	@media ${device.tablet} {
    	position: absolute;
		bottom: 1.5rem;
		left: calc(50% - 5rem);
    }
`;

const AdditionalInfo = styled.p`
	margin: .5rem 0 0 0;
`;

const HoverImage = styled(Image)`
	&:hover {
		transform: scale(1.03, 1.03);
	}
`;

const RemoveUser = styled.button`
	position: absolute;
	background-color: transparent;
    border: 0;
    cursor: pointer;
    outline: 0;
	right: .7rem;
    top: .7rem;
    padding: 0;
    
    @media ${device.tablet} {
    	display: none;
    }
`;

const RemoveImage = styled.img`
	width: 2rem;
	height: 2rem;
`;

const UserCard = styled.li`
    outline: #cdcfd2 solid .1rem;
    font-size: 1.5rem;
    background: #fff;
    position: relative;
    width: 100%;
    display: flex;
    align-items: center;
	padding: 1rem 1.5rem;
    box-sizing: border-box;
    height: 10rem;
	
	&.card-leave.card-leave-active {
	  	animation: ${leave} .6s cubic-bezier(.55,-0.04,.91,.94) forwards;
	}
    
    @media ${device.tablet} {
    	height: 22rem;
    	min-width: 15rem;
    	display: inline-block;
    	text-align: center;
    }
    
    &:hover {
    	background: #f3f6f8;
    	
    	${RemoveUser} {
    		display: inline-block;
    	}
    }
`;

const UserInfo = styled.div`
	flex: 1;
	display: flex;
	align-items: center;
`;

const WidthWrapper = styled.div`
	width: 100%;
	text-align: center;
`;

class User extends Component{

    onRemoveClick(id) {
		const userIndex = this.props.users.findIndex(user => user.id.value === id);
        let blacklist = localStorage['blacklisted'] ? JSON.parse(localStorage['blacklisted']) : [];
        const blacklistedDate = Date.now();

        blacklist = [...blacklist, {id, blacklistedDate}];
        localStorage['blacklisted'] = JSON.stringify(blacklist);
		this.props.addToBlacklist({userIndex, blacklistedDate});
	}

	render() {
		const {name, picture, dob, nat, id} = this.props;
		const { value } = id;
		const flagClasses = `flag-icon flag-icon-${nat.toLowerCase()}`;
		
		return (
			<UserCard>
				<RemoveUser onClick={this.onRemoveClick.bind(this, value)}>
					<RemoveImage src={crossImage} alt="remove"/>
				</RemoveUser>
				<UserInfo>
					<ImageNavLink to={`users/${value}`}>
						<HoverImage src={picture.large} alt={name.fullName}/>
					</ImageNavLink>
					<WidthWrapper>
						<StyledNavLink to={`users/${value}`}>{name.fullName}</StyledNavLink>
						<AdditionalInfo>
							<span className={flagClasses}></span> {getAge(dob)} years
						</AdditionalInfo>
					</WidthWrapper>
				</UserInfo>
				<ButtonWrapper>
					<ViewButton id={value}>View</ViewButton>
				</ButtonWrapper>
			</UserCard>
		);
	}
}

User.propTypes = {
    name: PropTypes.object.isRequired,
    picture: PropTypes.object.isRequired,
    dob: PropTypes.string.isRequired,
    nat: PropTypes.string.isRequired,
    id: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
	
	return {
		users: state.users
	}
};

const mapDispatchToProps = (dispatch) => {
	
	return {
		addToBlacklist: (userData) => dispatch(addToBlacklist(userData))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(User);