import React, { Fragment } from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

const SearchString = styled.span`
    font-size: 2.3rem;
    color: #0073b1;
`;

export const EmptyList = props => {
    const { searchValue } = props;

    if(searchValue) {
        return(
            <Fragment>
                No user(s) <SearchString>{searchValue}</SearchString> found, please correct search string or this user(s) is already in your
                <NavLink to={'/blacklist'}> Blacklist</NavLink>, or in <NavLink to={'/connections'}>Connections</NavLink>.
            </Fragment>
        );
    }

    return (
        <Fragment>
            List of people that you may know is empty at the moment. In order to fill it,
            you can clear your <NavLink to={'/blacklist'}>Blacklist</NavLink> or in
            <NavLink to={'/settings'}> Settings</NavLink> choose greater amount of downloaded people data.
        </Fragment>
    );
};