import React, { Component, Fragment } from 'react';
import User from './User';
import PropTypes from 'prop-types';
import { CSSTransitionGroup } from 'react-transition-group';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { EmptyList } from "./EmptyList";
import { setPager } from "../../actions/actions";

const HeaderText = props =>  props.amount ? 'People you may know' : <EmptyList searchValue={props.searchValue}/>;

const Title1 = styled.h1`
	font-weight: 500;
	color: rgba(0,0,0,.7);
	font-size: 1.8rem;
	background-color: #fff;
    margin: 1rem 0 0;
    padding: 1.2rem 1.6rem;
    text-align: justify;
`;

class UsersList extends Component {
    constructor(props) {
        super();
        this.state = {
            usersToRender: this.filterUsers(props)
        }
    }

    componentWillReceiveProps(nextProps) {
        const { users, searchValue } = nextProps;
        if(nextProps.users !== users || nextProps.searchValue !== searchValue) {
            const usersToRender = this.filterUsers(nextProps);

            this.setState({
                usersToRender
            });
        }
    }

    isUserSearched (user, searchValue) {
        return user.name.fullName.toLowerCase().search(searchValue) !== -1;
    }

    filterUsers(props) {
        const { users, searchValue, setPager, pager } = props;
        const usersToRender = users.filter(user => !(user.connected || user.blacklisted));
        const filteredUsers = searchValue ? usersToRender.filter(user => this.isUserSearched(user, searchValue)) : usersToRender;

        setPager(this.getPagerConfig(filteredUsers, pager));

        return filteredUsers;
    }

    getPagerConfig(filteredUsers, pager) {
        let startPage, lastPage;
        const { currentPage } = pager;
        const totalPages = Math.ceil(filteredUsers.length / pager.pageSize);
        const pages = Array.from(Array(totalPages), (e,i)=>i+1);

        switch (true) {
            case (totalPages <= 10 || currentPage <= 6):
                startPage = 1;
                lastPage = totalPages;
                break;
            case(currentPage + 4 >= totalPages): {
                startPage = totalPages - 9;
                lastPage = totalPages;
                break;
            }
            default:
                startPage = currentPage - 5;
                lastPage = currentPage + 4;

        }

        return {
            totalPages,
            startPage,
            lastPage,
            pages
        }
    }

   render() {
	   
    	const users = this.state.usersToRender.map((user) => {

			return <User key={user.dob} { ...user }/>
		});
    	const usersAmount = users.length;
    	
       return (
            <Fragment>
                <Title1>
                    <HeaderText amount={usersAmount} searchValue={this.props.searchValue}/>
                </Title1>
                <ul>
                    <CSSTransitionGroup
                        transitionName="card"
                        transitionEnter={false}
                        transitionLeaveTimeout={400}>
                        {users}
                    </CSSTransitionGroup>
                </ul>
            </Fragment>
       );
   }
}

const mapStateToProps = (state) => {
    const { pager, searchValue } = state;

    return {
        searchValue,
        pager
    }
};

const mapDispatchToProps = (dispath) => {

    return {
        setPager: (config) => dispath(setPager(config))
    }
};

UsersList.propTypes = {
    users: PropTypes.arrayOf(
        PropTypes.shape({
            //name: PropTypes.string.isRequired
        }).isRequired
    ).isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);