import React, { Component } from 'react';
import { Loader } from "./Loader";

export const LoaderHOC = (propName) => (WrappedComponent) => {
    return class LoaderHOC extends Component {
        isEmpty(prop) {
            return (
                prop === null
                || prop === undefined
                || (Array.isArray(prop) && prop.length === 0)
                || (prop.constructor === Object && Object.keys(prop).length === 0)
            );
        }
        render() {
            return (
                this.isEmpty(this.props[propName])
                    ? <Loader />
                    : <WrappedComponent {...this.props} />)
        }
    }
};