import styled, { keyframes } from 'styled-components';
import React from 'react';

const dotSize = 2;
const loaderWidth = 25;

const loadingText = keyframes`
	0% {
    	content: "Loading";
	}
	25% {
		content: "Loading.";
	}
	50% {
		content: "Loading..";
	}
	75% {
		content: "Loading...";
	}
`;

const loader = keyframes`
	15% {
    	transform: translateX(0);
  	}
  	45% {
    	transform: translateX(23rem);
  	}
  	65% {
    	transform: translateX(23rem);
  	}
  	95% {
    	transform: translateX(0);
  	}
`;


const LoaderContainer = styled.div`
	height: 100vh;
  	width: 100vw;
  	position: relative;
`;

const StyledLoader = styled.div`
	height: ${dotSize}rem;
  	width: ${loaderWidth}rem;
  	position: absolute;
  	bottom: 60%;
  	left: 0;
  	right: 0;
  	margin: auto;
`;

const LoaderDot = styled.div`
	animation: ${loader} ease-in-out 3s infinite;
    height: ${dotSize}rem;
  	width: ${dotSize}rem;
    border-radius: 100%;
    background-color: black;
    position: absolute;
    border: 2px solid white;
    
    &:first-child {
	  background-color: #8cc759;
	  animation-delay: 0.5s;
	}
	
	&:nth-child(2) {
	  background-color: #8c6daf;
	  animation-delay: 0.4s;
	}
	&:nth-child(3) {
	  background-color: #ef5d74;
	  animation-delay: 0.3s;
	}
	&:nth-child(4) {
	  background-color: #f9a74b;
	  animation-delay: 0.2s;
	}
	&:nth-child(5) {
	  background-color: #60beeb;
	  animation-delay: 0.1s;
	}
	&:nth-child(6) {
	  background-color: #fbef5a;
	  animation-delay: 0s;
	}
`;

const LoaderText = styled.div`
	position: absolute;
  	top: 200%;
  	left: 0;
  	right: 0;
  	width: 4rem;
  	margin: auto;
	
	&:after {
	  content: "Loading";
	  font-weight: bold;
	  font-size: 1.5rem;
	  animation: ${loadingText} 3s infinite;
	}
`;

export const Loader = () => {
	
	return <LoaderContainer>
			  <StyledLoader>
				  {
					  [...Array(6).keys()].map((el, key) => <LoaderDot key={key} />)
				  }
				  <LoaderText/>
			  </StyledLoader>
		   </LoaderContainer>
};