import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import loginBackground from '../../assets/img/friends.jpg';
import { Button } from "../StyledComponents/Buttons";
import { device } from "../../utils/mediaConst";

const LoginContainer = styled.div`
	background: url(${loginBackground}) center no-repeat;
    background-size: cover;
	padding: 8.4rem 2rem 30px;
    text-align: center;
	color: #2e567d;
	min-height: 100%;
	flex: 1 1 auto;
	
	@media ${device.phoneL} {
		padding-top: 12rem;
	}
	
	@media ${device.phoneLandScape} {
		padding-top: 9rem;
	}
	
	@media ${device.phoneLandScape} {
		padding-top: 15rem;
	}
`;

const Title1 = styled.h1`
	font-size: 3rem;
	text-transform: uppercase;
	margin: 0;
	text-shadow: .3rem .3rem #d6d3d3;
	
	@media ${device.phoneL} {
		margin-bottom: 3rem;
	}
	
	@media ${device.phoneLandScape} {
		margin-bottom: 1rem;
	}
	
	@media ${device.tablet} {
		font-size: 4rem;
	}
`;

const RedText = styled.span`
	color: #ff0000;
`;

const Text = styled.p`
	font-size: 1.7rem;
	text-shadow: .2rem .2rem #d6d3d3;
	
	@media ${device.tablet} {
		font-size: 2rem;
	}
`;

const SignIn = styled.div`
	max-width: 40rem;
    background: #f5f5f5;
    box-shadow: rgba(187, 187, 187, 1) 0 0 2rem -.1rem;
    font: 12px Arial, Helvetica, sans-serif;
    color: #666;
    border-radius: 10px;
    padding: 0 2rem 2rem;
	margin: 1rem auto 0;
	
	@media ${device.phoneL} {
		margin-top: 3rem;
	}
	
	@media ${device.phoneLandScape} {
		margin-top: 2rem;
	}
`;

const Title3 = styled.h3`
	font-size: 2rem;
	border-bottom: .1rem solid #bdbcbc;
	padding: 1rem 0;
	margin: 0 0 2rem;
`;

const StyledButton = styled(Button)`
	width: 15rem;
    height: 3rem;
    font-size: 1.5rem;
    line-height: 3rem;
    background-color: #24b6e4;
    color: white;
	margin: 1rem 0;
`;

const SignInText = styled.p`
	font-size: 1.5rem;
`;

class Login extends Component {
	
	
	callAuthentication() {
		this.props.auth0.authorize();
	}
	
	render() {

		return (
			<LoginContainer>
				<Title1>the social network for making <RedText>new friends</RedText></Title1>
				<Text>Millions of people are having fun and making new friends on <b>INFRIENDS</b> every day! You can too!</Text>
				<Text>It's free and always will be.</Text>
				<SignIn>
					<Title3>Join INFRIENDS!</Title3>
					<SignInText>If you already have an account, please Log In.</SignInText>
                    <StyledButton onClick={() => this.callAuthentication()}>Log In</StyledButton>
					<SignInText>If you don't have one yet, please Sign Up first.</SignInText>
                    <StyledButton>Sign Up</StyledButton>
				</SignIn>
			</LoginContainer>
		)
	}
}

function mapStateToProps(state) {
	
	return {
		auth0: state.auth.auth0
	}
}

export default connect(mapStateToProps)(Login);