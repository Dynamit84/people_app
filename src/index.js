import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import reducer from './reducers/combineReducers';
import './index.css';
import App from './App';
import {createBrowserHistory} from 'history';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';

const history = createBrowserHistory();
const middleware = routerMiddleware(history);

let store = createStore(reducer, applyMiddleware(middleware, thunk, logger));

render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
			<App />
		</ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);
