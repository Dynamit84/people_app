import React, { Component, Fragment } from 'react';
import UserDetailed from './components/Users/UserDetailed';
import { Route, Switch, withRouter } from 'react-router-dom';
import Home from './components/Home/Home';
import AppHeader from './components/Header/Header';
import { connect } from 'react-redux';
import { fetchUsers, changeSearchValue,toggleSearch } from "./actions/actions";
import MyConnections from './components/InteractedUsers/InteractedUsersList';
import { compose } from 'recompose';
import HandleAuth from "./components/Auth/HandleAuth";
import Login from "./components/Login/Login";
import { WithAuthentication } from "./components/Auth/requireAuthHOC";
import { WithoutAuthentication } from "./components/Auth/noRequireAuthHOC";
import { Footer } from "./components/Footer/Footer";

const blacklistConfig = {
	headerText: 'blacklisted',
	clearBtnText: 'blacklist',
	infoText: 'blacklisted',
	removeBtnText: 'whitelist',
	interactionDate: 'blacklistedDate'
};

const connectionsConfig = {
    headerText: 'connections',
    clearBtnText: 'connections',
    infoText: 'connected',
    removeBtnText: 'remove',
    interactionDate: 'connectionDate'
};

class App extends Component {

	componentDidMount() {
		this.getUsers();
	}

    shouldComponentUpdate(nextProps) {
		return nextProps.location.pathname !== this.props.location.pathname;
	}

	componentWillReceiveProps(nextProps) {
		const { location, changeSearchValue, searchValue, toggleSearch } = this.props;
		if (searchValue && nextProps.location.pathname !== location.pathname) {
            changeSearchValue('');
            toggleSearch();

		}
	}

    componentDidUpdate(prevProps) {
		this.getUsers(prevProps);
	}
	
	getUsers(prevProps) {
		const { users, isAuthenticated } = prevProps || this.props;
		if (!users.length && isAuthenticated) {
			this.props.fetchUsers();
		}
	}
	
	render() {

		return (
				<Fragment>
					<AppHeader location={this.props.location}/>
					<Switch>
						<Route exact={true} path='/' component={WithAuthentication()(Home)} />
						<Route path={'/home'} component={WithAuthentication()(Home)} />
						<Route path={'/login'} component={WithoutAuthentication(Login)} />
						<Route path={'/users/:id'} component={WithAuthentication()(UserDetailed)} />
						<Route path={'/connections'} component={WithAuthentication(connectionsConfig)(MyConnections)} />
						<Route path={'/blacklist'} component={WithAuthentication(blacklistConfig)(MyConnections)} />
						<Route path={'/callback'} component={HandleAuth}/>
						{/*<Route component={NotFound} />*/}
					</Switch>
					<Footer/>
				</Fragment>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	
	return {
		fetchUsers: () => dispatch(fetchUsers()),
        changeSearchValue: (value) => dispatch(changeSearchValue(value)),
		toggleSearch: () => dispatch(toggleSearch())
	}
};

const mapStateToProps = (state) => {

    return {
        users: state.users,
		isAuthenticated: state.auth.isAuthenticated,
		isLoading: state.isLoading,
        searchValue: state.searchValue
    }
};

export default compose( withRouter, connect(mapStateToProps, mapDispatchToProps))(App);
